import { createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';


function getLocalVue() {
  const localVue = createLocalVue();
  localVue.use(Vuex);

  return localVue;
}

export function mockStore(moduleName, options) {
  const localVue = getLocalVue();

  const store = new Vuex.Store(
    {
      modules: {
        [moduleName]: {
          namespaced: true,
          ...options,
        },
      },
    },
  );

  return { store, localVue };
}
