import { HttpResource } from '@/libs/resource';


export class AppResource extends HttpResource {
  url = 'http://localhost:3005/api';
}
