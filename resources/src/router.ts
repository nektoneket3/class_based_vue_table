import Vue from 'vue';
import Router from 'vue-router';

import Dashboard from './dashboard/views/Dashboard.vue';
import Details from './dashboard/views/Details.vue';
import NotFound from './components/not-found/NotFound.vue';


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
    },
    {
      path: '/details/:id',
      name: 'details',
      component: Details,
      props: true,
    },
    {
      path: '*',
      component: NotFound,
    },
  ],
});
