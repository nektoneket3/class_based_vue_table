import { GetMethod, Get, SendMethod, Post, Delete } from '@/libs/resource';
import { AppResource } from '@/services/app.resource';
import { TableRowMapper } from '@/store/modules/dashboard/mappers/table-row-mapper';
import { TableRowModel } from '@/store/modules/dashboard/models/table-row.model';


export class DashboardResource extends AppResource {

  path = '/table/rows';

  @Get({
    transformResponse: [
      JSON.parse,
      items => items.map(TableRowMapper.from),
    ],
  })
  getTableRows: GetMethod<TableRowModel[]>;

  @Get({
    path: '/{id}',
    transformResponse: [
      JSON.parse,
      item => item === null ? [] : [TableRowMapper.from(item)],
    ],
  })
  getTableRow: GetMethod<TableRowModel[]>;

  @Delete({ path: '/{id}' })
  remove: GetMethod<void>;

  @Post() // TODO add request mapping and change TableRowModel -> TableRowWebModel
  saveTableRows: SendMethod<TableRowModel[], void>;
}
