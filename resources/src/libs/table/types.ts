export type Asc = 'asc';
export type Desc = 'desc';

export type CellValue = string | number | null;
export type Component = 'StringCell' | 'NumberCell' | 'LinkCell';

export interface TableOrder {
  name: string,
  order: Asc | Desc;
}

export interface TableColumn {
  key: string;
  type?: 'string' | 'number' | 'link';
  label?: string | null;
  sortable?: boolean;
  editable?: boolean;
  validation?: RegExp;
  pattern?: string
}

export interface TableRow {
  [name: string]: CellValue,
}
