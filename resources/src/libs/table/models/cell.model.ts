import { Component } from '../types';


export interface CellConfig {
  key: string;
  editable?: boolean;
  validation?: RegExp;
}

export abstract class CellModel {
  abstract component: Component;
  abstract value;
  key: string;
  editable: boolean;
  validation: RegExp | null;

  protected constructor(config: CellConfig) {
    this.key = config.key;
    this.editable = config.editable || false;
    this.validation = config.validation || null;
  }

  isValid(value: string | null) {
    if (value === null) {
      return true;
    }
    if (this.validation) {
      return this.validation.test(value);
    }
    return true;
  }

  setValue(value) {
    this.value = value;
  }
}
