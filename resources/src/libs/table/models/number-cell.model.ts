import { toNumber } from 'lodash';

import { CellModel, CellConfig } from './cell.model';
import { Component } from '../types';


type NumberCellValue = number | null;

export interface NumberCellConfig extends CellConfig {
  value: NumberCellValue;
}

export class NumberCellModel extends CellModel {
  component: Component;
  value: NumberCellValue;

  constructor(config: NumberCellConfig) {
    super(config);

    this.component = 'NumberCell';
    this.value = config.value;
    this.validation = config.validation || /^\d+$/;
  }

  setValue(value: string) {
    if (value === '') {
      this.value = null;
    } else {
      this.value = toNumber(value);
    }
  }
}
