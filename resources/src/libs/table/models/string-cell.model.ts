import { Component } from '../types';
import { CellModel, CellConfig } from './cell.model';


export interface StringCellConfig extends CellConfig {
  value: string;
}

export class StringCellModel extends CellModel {
  component: Component;
  value: string;

  constructor(config: StringCellConfig) {
    super(config);
    this.component = 'StringCell';
    this.value = config.value;
  }
}
