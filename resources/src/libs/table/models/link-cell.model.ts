import { CellModel, CellConfig } from './cell.model';
import { Component } from '@/libs/table';


type LinkCellValue = {
  name: string,
  params: {
    [key: string]: string
  }
}

export interface LinkCellConfig extends CellConfig {
  value: LinkCellValue;
}

export class LinkCellModel extends CellModel {
  component: Component;
  value: LinkCellValue;

  constructor(config: LinkCellConfig) {
    super(config);

    this.component = 'LinkCell';
    this.value = config.value;
  }
}
