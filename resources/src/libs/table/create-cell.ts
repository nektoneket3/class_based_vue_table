import { CellValue, TableColumn } from './types';
import * as models from './models';


export function createCell(row: any, column: TableColumn): models.CellModel {

  const key: string = column.key;
  const value: CellValue = row[key];
  const editable = column.editable;
  const validation = column.validation;
  let link;

  switch (column.type) {
    default:
    case 'string':
      return new models.StringCellModel({
        key,
        value: (value as string),
        editable,
        validation,
      });

    case 'number':
      return new models.NumberCellModel({
        key,
        value: (value as number | null),
        editable,
        validation,
      });

    case 'link':
      if (column.pattern) {
        const [name, param] = column.pattern.split('/');
        link = { name, params: { [param]: row[param] } };
      }
      return new models.LinkCellModel({
        key,
        value: link,
      });
  }
}
