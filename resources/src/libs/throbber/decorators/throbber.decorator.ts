import { isEmpty } from 'lodash';
import { promiseStatus } from 'promise-status-async';


type Decorator = (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;

export class Throbber {
  status: boolean;
  private disabled: boolean;

  constructor(status = false) {
    this.status = status;
    this.disabled = status;
  }

  run() {
    this.status = true;
  }

  stop() {
    this.status = false;
  }

  disable() {
    this.disabled = true;
  }

  enable() {
    this.disabled = false;
  }

  static on({ throbberName = 'throbber', delay = 0 } = {}): Decorator {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor): void => {
      const originalMethod = descriptor.value;

      descriptor.value = function newMethod(): Promise<any> {
        // check if throbber exists and if not create it.
        if (isEmpty(this[throbberName])) {
          this[throbberName] = new Throbber();
        }
        // check if throbber is in progress right now and stop code running if true.
        if (this[throbberName].disabled) {
          return Promise.resolve();
        }

        const promise = originalMethod.apply(this, arguments);

        if (!(promise instanceof Promise)) {
          throw new Error('throbberDecorator: Decorated function should return a promise.');
        }
        // make decorated method not callable
        this[throbberName].disable();

        setTimeout(async () => {

          if (await promiseStatus(promise) !== 'pending') {
            return this[throbberName].enable();
          }

          // run public throbber
          this[throbberName].run();

          promise.finally(() => {
            this[throbberName].stop();
            this[throbberName].enable();
          });

        }, delay);

        return promise;
      };
    };
  }
}
