import { RequestMethod, HttpMethods } from './declarations';


export const httpMethods: HttpMethods = {
  get: 'GET',
  delete: 'DELETE',
  post: 'POST',
  put: 'PUT',
  patch: 'PATCH',
  head: 'HEAD',
  options: 'OPTIONS',
  trace: 'TRACE',
  connect: 'CONNECT',
};

export function isSendMethod(method: RequestMethod): boolean {
  const submitMethods: any[] = [
    httpMethods.post,
    httpMethods.put,
    httpMethods.patch,
  ];
  return submitMethods.includes(method);
}
