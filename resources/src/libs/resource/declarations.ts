import { AxiosRequestConfig, Method } from 'axios';


export type GET = 'GET';
export type POST = 'POST';
export type PUT = 'PUT';
export type PATCH = 'PATCH';
export type DELETE = 'DELETE';
export type OPTIONS = 'OPTIONS';
export type HEAD = 'HEAD';
export type TRACE = 'TRACE';
export type CONNECT = 'CONNECT';

export interface HttpMethods {
  get: GET;
  post: POST;
  put: PUT;
  patch: PATCH;
  delete: DELETE;
  options: OPTIONS;
  head: HEAD;
  trace: TRACE;
  connect: CONNECT;
}

export type RequestBodyType = 'json' | 'formData';
export type RequestMethod = Method;
export type RequestResponse<RR> = Promise<RR>;

export interface IRequestBody {
  [name: string]: any;
}

export type RequestBody = IRequestBody | null;

export interface RequestParams {
  [key: string]: string | number;
}

export interface RequestHeaders {
  [name: string]: string | string[];
}

export interface ResourceConfig {
  intercept?: boolean;
}
export interface DefaultResourceConfig {
  readonly intercept?: boolean;
}

export interface RequestOptions extends AxiosRequestConfig, ResourceConfig {
  headers?: RequestHeaders;
  path?: string;
  pathParams?: RequestParams;
  queryParams?: RequestHeaders;
  bodyType?: RequestBodyType;
}

export interface IResource {
  request<RB, RR>(method: RequestMethod, body?: RB, options?: RequestOptions): RequestResponse<RR>
}

export type SendMethod<RB, RR> = (requestBody: RB, requestOptions?: RequestOptions) => RR;

export type GetMethod<RR> = (requestOptions?: RequestOptions) => RR;
