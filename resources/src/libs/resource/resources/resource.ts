import axios, { AxiosResponse } from 'axios';

import { buildUrl } from '../helpers';
import { resourceConfig } from '../resource.config';
import {
  RequestOptions, ResourceConfig, RequestResponse, RequestMethod, RequestParams,
} from '../declarations';


export abstract class Resource {
  abstract url: string;
  path = '/';
  config: ResourceConfig = {};

  request<RB, RR>(
    method: RequestMethod,
    body?: RB,
    options: RequestOptions = {},
  ): RequestResponse<RR> {

    return axios.request({
      method,
      url: this.getUrl(options.path, options.pathParams),
      data: body,
      ...this.getRequestOptions(options),
    }).then((httpResponse: AxiosResponse<RR>) => httpResponse.data);
  }

  private getUrl(path: string = '', pathParams: RequestParams = {}): string {

    if (!this.url) {
      throw new Error('No URL provided');
    }

    const [erroredParams, url] = buildUrl(`${this.url}${this.path}${path}`, pathParams);

    if (erroredParams) {
      throw new Error(`Params: ${erroredParams.join(', ')} have not been passed`);
    }

    if (!url) {
      throw new Error('Incorrect provided url.');
    }

    return url;
  }

  private getRequestOptions<T>(requestOptions: RequestOptions): RequestOptions | T {
    return {
      ...resourceConfig,
      ...this.config,
      ...requestOptions,
    };
  }
}
