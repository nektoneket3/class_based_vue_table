import { Method } from 'axios';
import { RequestOptions, IResource } from '../declarations';
import { createMethod } from './create-method';


export function getDecorator(method: Method) {
  return (options?: RequestOptions) => (resource: IResource, methodName: string): void => {
    resource[methodName] = createMethod(method, options);
  };
}
