import { httpMethods } from '../http-methods';
import { getDecorator } from './get-decorator';


export const Post = getDecorator(httpMethods.post);
export const Get = getDecorator(httpMethods.get);
export const Patch = getDecorator(httpMethods.patch);
export const Put = getDecorator(httpMethods.put);
export const Delete = getDecorator(httpMethods.delete);
