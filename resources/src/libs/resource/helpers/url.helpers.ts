import { RequestParams } from '../declarations';


type ErroredParams = string[] | null;
type BuiltUrl = string | null;

export function buildUrl(
  url: string,
  pathParams: RequestParams,
  paramsRegex = /{[(a-z)]\w+}/g,
): [ErroredParams, BuiltUrl] {

  const paramsFromUrl: RegExpMatchArray | null = url.match(paramsRegex);

  if (!paramsFromUrl) {
    return [null, url];
  }

  const erroredParams: ErroredParams = paramsFromUrl.reduce(
    (acc: string[], param: string): string[] => {
      param = param.replace(/[{}]/g, '');
      const paramValue: string | number = pathParams[param];

      if (paramValue) {
        url = url.replace(paramsRegex, `${paramValue}`);
      } else {
        acc.push(param);
      }

      return acc;
    }, []);

  if (erroredParams && erroredParams.length) {
    return [erroredParams, null];
  }

  return [null, url];
}
