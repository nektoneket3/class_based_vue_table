export interface TableRowWebModel {
  id: string;
  name: string;
  location: string;
  currency: number | null;
  _id?: string | null;
}
