import { Mapper } from '@/store/core/mapper';
import { TableRowWebModel } from '@/store/modules/dashboard/models/table-row-web.model';
import { TableRowModel } from '@/store/modules/dashboard/models/table-row.model';


export const TableRowMapper: Mapper<TableRowWebModel, TableRowModel> = {

  from(param: TableRowWebModel): TableRowModel {
    return {
      id: param.id,
      name: param.name,
      location: param.location,
      currency: param.currency,
      _id: param._id || null,
    };
  },

  to(param: TableRowModel): TableRowWebModel {
    return {
      id: param.id || '',
      name: param.name,
      location: param.location,
      currency: param.currency,
      _id: param._id || null,
    };
  },
};
