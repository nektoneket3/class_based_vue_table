import { GetterTree } from 'vuex';
import { orderBy, omit, clone } from 'lodash';

import { RootState } from '@/store/core/root-state';
import { DashboardState } from '@/store/modules/dashboard/state';
import { TableRowModel } from '@/store/modules/dashboard/models/table-row.model';


export const getters: GetterTree<DashboardState, RootState> = {

  filteredTableRows({ tableRows, filterString, tableOrder }): TableRowModel[] {
    return orderBy(
      filterString
        .split(' ')
        .filter(searchWord => !!searchWord)
        .reduce(
          (acc: TableRowModel[], searchWord: string) => {
            return acc.filter((item: TableRowModel) => {
              const itemSearchFields: string = Object.values(omit(item, 'id')).join(' ');
              return itemSearchFields.toLowerCase().includes(searchWord.toLowerCase());
            });
          },
          clone(Object.values(tableRows)),
        ),
      [tableOrder.name],
      [tableOrder.order],
    );
  },

  totalCurrency({ tableChangedRows }, { filteredTableRows }): number {
    return filteredTableRows.reduce(
      (total: number, row: TableRowModel) => {
        const changedRow = tableChangedRows[row.id];
        const currency = changedRow ? changedRow.currency : row.currency;
        total += currency || 0;
        return total;
      },
      0,
    );
  },

  totalTableChangedRows({ tableChangedRows }): number {
    return Object.keys(tableChangedRows).length;
  },

  totalTableRows(state, { filteredTableRows }): number {
    return filteredTableRows.length;
  },
};
