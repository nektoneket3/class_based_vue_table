import { ActionTree } from 'vuex';

import { RootState } from '@/store/core/root-state';
import { DashboardState } from '@/store/modules/dashboard/state';
import { DashboardResource } from '@/dashboard/services/dashboard.resource';


const resource = new DashboardResource();

export const actions: ActionTree<DashboardState, RootState> = {

  async getTableRows({ commit, dispatch }, id?: string): Promise<void> {
    dispatch('resetTable');
    commit('setFilterString', '');
    try {
      commit(
        'setTableRows',
        id
          ? await resource.getTableRow({ pathParams: { id: (id as string) } })
          : await resource.getTableRows(),
      );
    } catch (error) {
      commit('getTableRowsError', error);
    }
  },

  async saveTableRows({ commit, state }): Promise<void> {
    try {
      await resource.saveTableRows(Object.values(state.tableChangedRows));
      commit('updateTable');
      commit('resetTableChangedRows');
    } catch (error) {
      console.log(error);
    }
  },

  async removeTableRow({ commit, dispatch, state }, id): Promise<void> {
    try {
      await resource.remove({ pathParams: { id: (id as string) } });
      dispatch('resetTable');
    } catch (error) {
      console.log(error);
    }
  },

  resetTable({ commit }) {
    commit('resetTableChangedRows');
    commit('setTableRows', []);
  }

};
