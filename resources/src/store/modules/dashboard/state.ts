import { TableRowModel } from '@/store/modules/dashboard/models/table-row.model';
import { TableOrder } from '@/libs/table';


type TableRows = { [name: string]: TableRowModel };

export interface DashboardState {
  tableRows: TableRows,
  tableChangedRows: TableRows,
  tableOrder: TableOrder;
  filterString: string;
  errors: any[];
}

export const state: DashboardState = {
  tableRows: {},
  tableChangedRows: {},
  tableOrder: { name: 'name', order: 'asc' },
  filterString: '',
  errors: [],
};
