import { MutationTree } from 'vuex';

import { DashboardState } from '@/store/modules/dashboard/state';
import { TableRowModel } from '@/store/modules/dashboard/models/table-row.model';
import { TableOrder } from '@/libs/table';


export const mutations: MutationTree<DashboardState> = {

  setTableRows(state, rows: TableRowModel[]) {
    state.tableRows = rows.reduce(
      (acc, row: TableRowModel) => ({ ...acc, [row.id]: row }),
      {},
    );
  },

  updateTable(state) {
    state.tableRows = { ...state.tableRows, ...state.tableChangedRows };
  },

  setChangedTableRow(state, row: TableRowModel) {
    state.tableChangedRows = { ...state.tableChangedRows, [row.id]: row };
  },

  resetTableChangedRows(state) {
    state.tableChangedRows = {};
  },

  getTableRowsError(state, error: any) {
    state.errors = error;
  },

  setTableOrder(state, tableOrder: TableOrder) {
    state.tableOrder = tableOrder;
  },

  setFilterString(state, filterString: string) {
    state.filterString = filterString;
  },

};
