import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

import { RootState } from '@/store/core/root-state';
import dashboard from './modules/dashboard';


Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  state: {
    version: '1.0.0',
  },
  modules: {
    dashboard,
  },
};

export default new Vuex.Store<RootState>(store);
