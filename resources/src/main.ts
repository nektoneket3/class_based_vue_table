import Vue from 'vue';
import App from './App.vue';

import 'bootstrap/scss/bootstrap.scss';

import router from './router';
import store from './store';

import Table from './libs/table';
import Throbber from './libs/throbber';
import DashboardTable from './dashboard/components/DashboardTable.vue';
import Button from './components/button/Button.vue';
import SaveButton from './components/save-button/SaveButton.vue';


Vue.component('Table', Table);
Vue.component('Throbber', Throbber);
Vue.component('DashboardTable', DashboardTable);
Vue.component('Button', Button);
Vue.component('SaveButton', SaveButton);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
