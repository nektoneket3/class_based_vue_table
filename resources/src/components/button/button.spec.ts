import { shallowMount } from '@vue/test-utils';

import Button from './Button.vue';


describe('Button.vue', () => {
  [
    [undefined, 'button is not disabled by default', null],
    [true, 'button is disabled when passed true', 'disabled'],
    [false, 'button is not disabled when passed false', null],
  ]
    .forEach(([disabled, description, expectation]) => {
      it((description as string), () => {

        const wrapper = shallowMount(
          Button,
          { propsData: { disabled }}
        );

        const button = wrapper.find('button');
        const disabledAttrValue = button.element.getAttribute('disabled');

        expect(disabledAttrValue).toEqual(expectation);
      });
    });
});
