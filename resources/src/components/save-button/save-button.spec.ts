import { shallowMount } from '@vue/test-utils';

import { mockStore } from '@/services/mock-store';
import { storeName } from '@/store/modules/dashboard';
import SaveButton from './SaveButton.vue';


describe('SaveButton.vue', () => {

  let mocks;
  let actions;
  let getters;

  beforeEach(() => {
    actions = { saveTableRows: jest.fn() };
    getters = { totalTableChangedRows: () => 1 };
    mocks = mockStore(storeName, { actions, getters });
  });

  it('calls store action "saveTableRows" when button is clicked', () => {
    const wrapper = shallowMount(SaveButton, mocks);
    wrapper.find('Button').trigger('clicked');
    expect(actions.saveTableRows).toHaveBeenCalled();
  });
});
