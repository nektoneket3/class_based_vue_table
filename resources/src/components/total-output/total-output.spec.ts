import { shallowMount } from '@vue/test-utils';

import TotalOutput from './TotalOutput.vue';


describe('TotalOutput.vue', () => {

  it('renders props', () => {
    const label = 'label';
    const total = '1';

    const wrapper = shallowMount(TotalOutput, {
      propsData: { label, total },
    });

    expect(wrapper.find('#total-output-label').text()).toMatch(label);
    expect(wrapper.find('#total-output-total').text()).toMatch(total);
  })
});
