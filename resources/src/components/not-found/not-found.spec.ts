import { mount } from '@vue/test-utils';

import NotFound from './NotFound.vue';


describe('NotFound.vue', () => {
  it('renders exact text', () => {
    const text = 'Page not found';
    const wrapper = mount(NotFound);
    expect(wrapper.text()).toMatch(text);
  });
});
