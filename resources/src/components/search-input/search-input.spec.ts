import { shallowMount } from '@vue/test-utils';

import SearchInput from './SearchInput.vue';


describe('SearchInput.vue', () => {

  it('emits an event with input text after passed props.debounce', (done) => {
    const debounce = 100;
    const inputText = 'input';

    const wrapper = shallowMount(SearchInput, {
      propsData: { debounce },
    });

    const input = wrapper.find('input');
    (input.element as any).value = inputText;
    input.trigger('input');

    window.setTimeout(() => {
      expect(wrapper.emitted('changed')[0]).toEqual([inputText]);
      done();
    }, debounce);
  })
});
