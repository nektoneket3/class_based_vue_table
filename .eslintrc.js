module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'import/prefer-default-export': 'off',
    'padded-blocks': 'off',
    'function-paren-newline': 'off',
    'prefer-rest-params': 'off',
    'no-param-reassign': 'off',
    'prefer-destructuring': 'off',
    'lines-between-class-members': 'off',
    'object-curly-newline': 'off',
    'max-len': ['error', { 'code': 120 }],
    'arrow-body-style': 'off',
    'default-case': 'off',
    'class-methods-use-this': 'off',
    'no-underscore-dangle': 'off',
  },
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
      ],
      env: {
        mocha: true,
      },
    },
  ],
};
