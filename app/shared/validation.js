const validate = require('express-validation');


module.exports = (rules) => {
  return [
    validate(rules),
    (error, request, response, next) => next(error),
  ];
};
