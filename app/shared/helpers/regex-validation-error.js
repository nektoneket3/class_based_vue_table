module.exports = function regexValidationError(errors, regexName) {
  return errors.map((error) => {
    if (error.type === 'string.regex.base') {
      return {
        template: 'fails to match "{{regexName}}" pattern',
        context: { regexName },
      };
    }
    return error;
  });
};
