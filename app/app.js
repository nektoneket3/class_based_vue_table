const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config');


const app = express();

app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json());
app.use(cors());

app.use(`${config.apiPrefix}/table`, require('./table/table.routes'));

app.use(`${config.apiPrefix}`, require('./core/middlewares/error-handler.middleware'));

module.exports = app;
