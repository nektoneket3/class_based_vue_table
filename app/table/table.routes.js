const express = require('express');

const TableController = require('./table.controller');


const router = express.Router();
const tableController = new TableController();
const path = '/rows';

router.get(path, tableController.list.bind(tableController));
router.get(`${path}/:id`, tableController.retrieve.bind(tableController));
router.delete(`${path}/:id`, tableController.delete);
router.post(path, tableController.create);

module.exports = router;
