const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const deletedTableRowSchema = new Schema({
  id: {
    type: Schema.Types.String,
    unique: true,
  },
});

module.exports = mongoose.model('DeletedTableRow', deletedTableRowSchema);
