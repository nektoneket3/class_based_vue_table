const { unionBy } = require('lodash');
const statuses = require('http-status-codes');

const initialRows = require('./test');
const TableRow = require('./table-row.model');
const DeletedTableRow = require('./deleted-table-row.model');


module.exports = class TableController {

  async list(req, res) {
    const savedRows = await TableRow.find();
    const resultRows = unionBy(savedRows, initialRows, 'id');

    res.json(
      await this.excludeDeletedRows(resultRows),
    );
  }

  async create(req, res) {
    const rows = req.body.map(async (row) => {
      let _row;
      if (row._id) {
        _row = await TableRow.findByIdAndUpdate(
          row._id,
          { $set: row },
          { new: true, useFindAndModify: false },
        );
      } else {
        _row = new TableRow(row);
      }

      return _row.save();
    });

    const savedRows = await Promise.all(rows);

    res.json(savedRows);
  }

  async retrieve(req, res) {
    let retrieveRow = await TableRow.findOne({ id: req.params.id })
      || initialRows.find(tableRow => tableRow.id === req.params.id);

    retrieveRow = await this.excludeDeletedRows([retrieveRow]);

    res.json(retrieveRow[0] || null);
  }

  async delete(req, res) {
    try {
      const deletedRow = new DeletedTableRow({ id: req.params.id });
      await DeletedTableRow.init();
      await deletedRow.save();
    } catch (error) {
      console.log(error);
    }

    res.status(statuses.NO_CONTENT).json();
  }

  async excludeDeletedRows(rows) {
    const deletedRows = await DeletedTableRow.find();
    return rows.filter(
      row => !deletedRows.find(deletedRow => deletedRow.id === row.id),
    );
  }

};
