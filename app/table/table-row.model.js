const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const tableRowSchema = new Schema({
  id: {
    type: String,
  },
  name: {
    type: String,
  },
  location: {
    type: String,
  },
  currency: {
    type: Number,
  },
});

module.exports = mongoose.model('TableRow', tableRowSchema);
