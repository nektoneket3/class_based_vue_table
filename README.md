## Clone Project
```
git clone https://gitlab.com/nektoneket3/class_based_vue_table.git
```

## Project setup
Make sure that yarn and mongoDB installed and running.

### Install dependencies, run tests, compile app and run dev servers
```
yarn deploy

```
